{:ok, input} = File.read("input.txt")
lines = input |> String.split("\n\n", trim: true)

defmodule Day4 do
	def valid_passport(passport) do

		required_keys = [
			"byr", #(Birth Year)
			"iyr", #(Issue Year)
			"eyr", #(Expiration Year)
			"hgt", #(Height)
			"hcl", #(Hair Color)
			"ecl", #(Eye Color)
			"pid", #(Passport ID)
		]

		data = passport
			|> String.replace("\n", " ")
			|> String.split(" ")
			|> Enum.map(fn x ->
				[key, value] = String.split(x, ":", trim: true)
				%{key => value}
			end)
			|> Enum.reduce(fn x, y ->
			   Map.merge(x, y, fn _k, v1, v2 -> v2 ++ v1 end)
			end)

		#IO.inspect data
		Enum.all?(required_keys, fn key -> data[key] end)
	end
end

number_of_valid_passports = Enum.map(lines, &Day4.valid_passport/1) |> Enum.count(&(&1))
IO.puts "There is #{number_of_valid_passports} number of valid passports"
