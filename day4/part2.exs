{:ok, input} = File.read("input.txt")
lines = input |> String.split("\n\n", trim: true)

defmodule Day4 do
	def valid_passport(passport) do

		required_values = %{
			"byr" => fn val -> 
				#(Birth Year)
				length = String.length(val)
				byr = String.to_integer(val)
				length == 4 && byr >= 1920 && byr <= 2002
			end,
			"iyr" => fn val -> 
				#(Issue Year)
				length = String.length(val)
				iyr = String.to_integer(val)
				length == 4 && iyr >= 2010 && iyr <= 2020
			end,
			"eyr" => fn val -> 
				 #(Expiration Year)
				 length = String.length(val)
				 eyr = String.to_integer(val)
				 length == 4 && eyr >= 2020 && eyr <= 2030 
			end,
			"hgt" => fn val -> 
				#(Height)
				length = String.length(val)
				if (length > 0 && (String.contains?(val, "cm") || String.contains?(val, "in"))) do
					if (String.contains?(val, "cm")) do
						cm = String.replace(val, "cm", "") |> String.to_integer
						cm >= 150 && cm <= 193
					else
						inch = String.replace(val, "in", "") |> String.to_integer
						inch >= 59 && inch <= 76
					end
				else
					false
				end
			end,
			"hcl" => fn val ->
				 #(Hair Color)
				 Regex.match?(~r/#([0-9]|[a-f]){6}/, val)
			end,
			"ecl" => fn val ->
				#(Eye Color)
				Enum.member?(["amb", "blu", "brn", "gry", "grn", "hzl", "oth"], val)
			end,
			"pid" => fn val ->
				#(Passport ID)
				length = String.length(val)
				only_digits = Regex.match?(~r{\A\d*\z}, val)
				length == 9 && only_digits
			end
		}

		data = passport
			|> String.replace("\n", " ")
			|> String.split(" ")
			|> Enum.map(fn x ->
				[key, value] = String.split(x, ":", trim: true)
				%{key => value}
			end)
			|> Enum.reduce(fn x, y ->
			   Map.merge(x, y, fn _k, v1, v2 -> v2 ++ v1 end)
			end)

		#IO.inspect data
		Enum.all?(required_values, fn({key, func}) ->
			val = data[key]
			val && func.(val)
		end)
	end
end

number_of_valid_passports = Enum.map(lines, &Day4.valid_passport/1) |> Enum.count(&(&1))
IO.puts "There is #{number_of_valid_passports} number of valid passports"
