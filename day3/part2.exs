{:ok, input} = File.read("input.txt")
lines = input |> String.split("\n", trim: true)

defmodule AoC do
	require Integer
	def find_trees([head | tail], index, trees, line, right, down) do
		
		head = if (index >= String.length(head)), do: String.duplicate(head, round(index / String.length(head)) + 1), else: head
		should_count = if down == 2, do: Integer.is_odd(line), else: true
		index = if down == 2 && should_count && line != 1, do: index - right, else: index
		char = String.at(head, index)

		#IO.inspect({char, index, index + 3, head})
		IO.puts "Running for line #{line} (should count = #{should_count}) and current index is #{index} and current char is #{char}, right #{right} down #{down}"

		if (line != 1 && char == "#" && should_count) do
			find_trees(tail, index + right, trees + 1, line + 1, right, down)
		else
			find_trees(tail, index + right, trees, line + 1, right, down)
		end
	end

	def find_trees([], _, trees, _, _, _) do
		trees
	end
end

slopes = [
	{1, 1},
	{3, 1},
	{5, 1},
	{7, 1},
	{1, 2}
]

total_tree_list = Enum.map slopes, fn(slope) ->
	right = elem(slope, 0)
	down = elem(slope, 1)
	AoC.find_trees(lines, 0, 0, 1, right, down)
end


IO.inspect total_tree_list
total_trees = Enum.reduce(total_tree_list, 1, &(&1 * &2))
IO.puts("Number of trees for all slopes: #{total_trees}")