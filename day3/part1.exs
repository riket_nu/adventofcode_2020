defmodule AoC do
	def find_trees([head | tail], index, trees, line) do
		
		head = if (index > String.length(head)), do: String.duplicate(head, round(index / String.length(head)) + 1), else: head

		char = String.at(head, index)

		#IO.inspect({char, index, index + 3, head})
		IO.puts "Running for line #{line} and current index is #{index} and current char is #{char}"

		if (line != 1 && char == "#") do
			find_trees(tail, index + 3, trees + 1, line + 1)
		else
			find_trees(tail, index + 3, trees, line + 1)
		end
	end

	def find_trees([], _, trees, _) do
		trees
	end
end

number_of_trees = File.stream!("input.txt")
	|> Enum.map(&String.trim_trailing/1)
	|> AoC.find_trees(0, 0, 1)

IO.puts("Number of trees: #{number_of_trees}")