{:ok, input} = File.read("example.txt")
lines = input
	|> String.split("\n", trim: true)
	|> Enum.map(&String.to_integer/1)

defmodule Day1 do
	def find_2020([head | tail], lines) do
		second_number = Enum.find(tail, fn a ->
			Enum.find(lines, fn b -> 
				b === 2020 - head - a
			end)
		end)

		case second_number do
			nil -> find_2020(tail, lines)
			_ -> get_third_number(head, second_number, lines)
		end
	end

	def get_third_number(head, second_number, lines) do
		third_number = Enum.find(lines, fn n -> 
			n === 2020 - head - second_number
		end)
		[head, second_number, third_number]
	end
end

numbers = Day1.find_2020(lines, lines)

IO.inspect numbers
total = Enum.reduce(numbers, 1, fn n, acc ->
	n * acc
end)

IO.puts "Final result is: #{total}"