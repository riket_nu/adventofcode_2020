defmodule AoC do
	def find_sum([head | tail], sum_to_find) do
		number = Enum.find(tail, fn a ->
			a === sum_to_find - head 
		end)
		if number == nil do	
			find_sum(tail, sum_to_find)
		else
			{head, number}
		end
	end

	def find_sum_two([head | tail], sum_to_find) do
		number = Enum.find(tail, fn a ->
			a === sum_to_find - head 
		end)
		if number == nil do	
			find_sum(tail, sum_to_find)
		else
			{head, number}
		end
	end
end

data = File.stream!("example.txt")
	|> Stream.map(&String.trim_trailing/1)
	|> Enum.map(&String.to_integer/1)
	|> AoC.find_sum(2020)

IO.inspect(data)
IO.inspect(elem(data, 0) * elem(data, 1))