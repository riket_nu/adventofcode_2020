{:ok, input} = File.read("input.txt")
lines = input |> String.split("\n", trim: true)

defmodule Day2 do
	def is_valid_pw(policy_and_password_string) do
		[policy, password] = policy_and_password_string |> String.split(":", trim: true)
		[minmax, char] = policy |> String.split(" ", trim: true)
		[min, max] = minmax |> String.split("-", trim: true) |> Enum.map(&String.to_integer/1)
		password = String.trim(password)
		[min_char, max_char] = [String.at(password, min - 1), String.at(password, max - 1)]
		(min_char == char || max_char == char) && !(min_char == char && max_char == char)
	end
end

num_of_valid_pws = Enum.map(lines, &Day2.is_valid_pw/1) |> Enum.count(&(&1))
IO.puts("There is #{num_of_valid_pws} valid passwords")