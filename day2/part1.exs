{:ok, input} = File.read("input.txt")
lines = input |> String.split("\n", trim: true)

defmodule Day2 do
	def is_valid_pw(policy_and_password_string) do
		[policy, password] = policy_and_password_string |> String.split(":", trim: true)
		[minmax, char] = policy |> String.split(" ", trim: true)
		[min, max] = minmax |> String.split("-", trim: true) |> Enum.map(&String.to_integer/1)
		password = String.trim(password)
		[head] = char |> to_charlist
		number_of_times = password |> to_charlist |> Enum.filter(&(&1 == head)) |> length
		IO.puts "The character #{char} occurres #{number_of_times} in password #{password}, it should occur min #{min} and max #{max} times"
		number_of_times >= min && number_of_times <= max
	end
end

num_of_valid_pws = Enum.map(lines, &Day2.is_valid_pw/1) |> Enum.count(&(&1))
IO.puts("There is #{num_of_valid_pws} valid passwords")